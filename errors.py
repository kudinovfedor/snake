class GameOverError(Exception):
    """
    Raise in case if you need to interrupt game loop
    """
