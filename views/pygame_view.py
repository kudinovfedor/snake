import pygame
import configs
from models import Field
from .view import View


BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
SOME = (255, 255, 0)

palette = {
    "default": WHITE,
    ".": SOME,
    "<+>": RED,
    "<X>": BLUE,
    "*": GREEN,
    "X": BLACK
}


class Segment(pygame.sprite.Sprite):
    """ Class to represent one segment of the snake. """

    # -- Methods
    # Constructor function
    def __init__(self, x, y, value):  # todo prepare views for each object
        # Call the parent's constructor
        super().__init__()

        # Set height, width
        self.image = pygame.Surface([
            configs.SEGMENT_WIDTH,
            configs.SEGMENT_HEIGHT
        ])
        self.image.fill(palette.get(value, palette["default"]))

        # Make our top-left corner the passed-in location.
        self.rect = self.image.get_rect()
        self.rect.x = x * configs.SEGMENT_WIDTH - configs.SEGMENT_MARGIN
        self.rect.y = y * configs.SEGMENT_HEIGHT - configs.SEGMENT_MARGIN


class PygameView(View):
    def __init__(self):
        # Call this function so the Pygame library can initialize itself
        pygame.init()
        # Create an 800x600 sized screen
        self.screen = pygame.display.set_mode([
            configs.DISPLAY_WIDTH,
            configs.DISPLAY_HEIGHT
        ])
        # Set the title of the window
        pygame.display.set_caption(configs.WINDOW_CAPTION)
        self.clock = pygame.time.Clock()

    def draw(self, field: Field):
        field_view = self.prepare_field_view(field)
        # todo draw snake view, draw frog view etc.

        # Clear screen
        self.screen.fill(BLACK)

        allspriteslist = pygame.sprite.Group()
        for _i, i in enumerate(field_view):
            for _j, j in enumerate(i):
                allspriteslist.add(Segment(_j+1, _i+1, value=j))
        allspriteslist.draw(self.screen)

        pygame.display.flip()
        # Pause
        self.clock.tick(configs.CLOCK_TICK)

        handle_tear_down()


def handle_tear_down():
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            raise KeyboardInterrupt
