from constants import *
from models import Field


class View:

    def draw(self, field: Field):
        field_view = self.prepare_field_view(field)
        print("=" * field.width)
        for i in field_view:
            print(i)
        print("=" * field.width)

    def prepare_field_view(self, field):
        field_view = [["." for _ in range(field.width)] for _ in range(field.height)]

        head_image_map = {
            LEFT: "<",
            RIGHT: ">",
            TOP: "/\\",
            BOTTOM: "\\/"
        }

        frog_images = {
            "Frog": "<+>",
            "BigFrog": "<X>"
        }

        for frog in field.frogs:
            if frog:  # if frog is active
                field_view[-frog.y][frog.x - 1] = frog_images[frog.__class__.__name__]

        for snake in field.snakes:
            snake_body = ["X", "*"]
            for x, y in snake:
                field_view[-y][x - 1] = snake_body[bool(snake)]

            snake_head_x, snake_head_y = snake.head
            field_view[-snake_head_y][snake_head_x - 1] = head_image_map[snake.orientation]

        return field_view
