from constants import (LEFT, RIGHT, BOTTOM, TOP,
                       CONSOLE_VIEW, PYGAME_VIEW,
                       AI_SNAKE, PLAYER_SNAKE,
                       SIMPLE_FIELD, TRANSIENT_FIELD)

# PLAYER_SNAKE works only with PYGAME_VIEW

ROUND_TIME = 500
TIMEOUT = 0.5

# field
FIELD_TYPE = SIMPLE_FIELD
FIELD_SIZE = 20, 10  # width, height
FIELD_FROGS_COUNT = 4

VIEW = PYGAME_VIEW

# Pygame View
# =================
DISPLAY_WIDTH = 800
DISPLAY_HEIGHT = 600
# Set the width and height of each snake segment
SEGMENT_WIDTH = 25
SEGMENT_HEIGHT = 25
# Margin between each segment
SEGMENT_MARGIN = 20  # todo check
CLOCK_TICK = 5
WINDOW_CAPTION = 'Snake Example'


SNAKES = [
    {
        "name": "Snake 1",
        "type": AI_SNAKE,
        "start_point": (1, 1),
        "start_length": 5,
        "start_orientation": RIGHT
    },
    {
        "name": "Snake 2",
        "type": AI_SNAKE,
        "start_point": (1, FIELD_SIZE[1]),
        "start_length": 5,
        "start_orientation": BOTTOM
    },
    {
        "name": "Snake 3",
        "type": AI_SNAKE,
        "start_point": (FIELD_SIZE[0], 1),
        "start_length": 5,
        "start_orientation": TOP
    },
    {
        "name": "Snake 3",
        "type": AI_SNAKE,
        "start_point": FIELD_SIZE,
        "start_length": 5,
        "start_orientation": LEFT
    }
]
