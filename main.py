import time

import configs
import views
from errors import GameOverError
from models import snakes, fields, Coords, Snake, Frog


class App:

    best_score = 0

    def __init__(self):
        self.view = getattr(views, configs.VIEW)()

    def run(self):
        try:
            while True:
                field = self.setup_game_session()
                result = self.run_game_session(field)
                self.process_game_session_result(result)
        except KeyboardInterrupt:
            print("Game is Over, your best score is: {}".format(self.best_score))
            raise GameOverError

    def setup_game_session(self):
        # setup
        field = getattr(fields, configs.FIELD_TYPE)(*configs.FIELD_SIZE)

        for snake_config in configs.SNAKES:
            snake = getattr(snakes, snake_config["type"])(name=snake_config["name"],
                                                          head_position=Coords(*snake_config["start_point"]),
                                                          length=snake_config["start_length"],
                                                          orientation=snake_config["start_orientation"])
            field.snakes.append(snake)
        return field

    def run_game_session(self, field):
        round_time = configs.ROUND_TIME

        try:
            while True:
                # view
                self.view.draw(field)
                time.sleep(configs.TIMEOUT)

                Frog.all_reduce_life_time(field.frogs)
                field.update_frogs()

                # prepare next move beforehand
                # so snakes could make move simultaneously
                Snake.all_prepare_next_move(field.active_snakes, field)
                Snake.all_make_moves(field.active_snakes)
                Snake.all_check_collisions(field.active_snakes, field)
                Snake.all_eat_frogs(field.active_snakes, field)

                # all snakes are dead or there is no room
                if not any(field.snakes) or not field.get_all_empty_cells():
                    raise GameOverError

                if round_time > 0:
                    round_time -= 1
                else:
                    raise GameOverError

        # todo process all snakes scores
        except GameOverError:
            print("Game is Over, your score is: {}".format("score"))
            return "score"

    def process_game_session_result(self, result):
        # todo sessions, game time, configs etc.
        # todo process all snakes scores
        return
        # if result > self.best_score:
        #     print("New Best Score {}".format(result))
        #     self.best_score = result
        #     with open("scores.txt", "a") as file:
        #         file.write(str(result))
        #         file.write("\n")


if __name__ == "__main__":
    App().run()
