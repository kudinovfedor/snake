import random
import pygame

from .fields import Field
from .helpers import Coords
from .frogs import Frog

from constants import *


class Snake:
    def __init__(self, head_position: Coords, length, orientation=RIGHT, name="Snake"):

        self.body = [head_position]
        self.orientation = orientation
        self.length = length
        self.name = name
        self.active = True
        self.score = 0
        self._next_move = None

    @property
    def head(self):
        return self[0]

    def next_move(self, field: Field):
        """
        get new coords based on field
        """
        next_move = field.get_next_position(snake=self)

        # snake can move in place where other snakes tail
        # but if it is dead it will not move
        # also take in account other snakes length, as it can still grow, if it's not dead of course
        # todo I should suppose other snake dead if it has nowhere to go
        if next_move not in field or any(next_move in snake[:snake.length - snake.active] for snake in field.snakes):
            return None
        return next_move

    def make_move(self):

        if not self or self._next_move is None:
            return

        self.body = self.body[:self.length-1]
        self.body.insert(0, self._next_move)

    def __iter__(self):
        yield from self.body

    def __str__(self):
        return str(self.body)

    def __bool__(self):
        return self.active

    def __getitem__(self, item):
        return self.body[item]

    def eat_frog(self, frog: Frog):
        if frog:
            self.length += frog.value
            frog.active = False

    def select_direction(self, field: Field):
        """
        self controlled snake
        :param field:
        :return:
        """
        # todo add controllers
        raise NotImplementedError

    def check_collisions(self, field: Field):
        if any(self.head in other_snake for other_snake in field.snakes if self is not other_snake):
            return True
        if self.head in self[1:]:
            return True
        if self.head not in field:
            return True
        return False

    def prepare_next_move(self, field):
        self._next_move = self.next_move(field)

    @classmethod
    def all_prepare_next_move(cls, snakes, field):
        for snake in snakes:
            snake.select_direction(field)
            snake.prepare_next_move(field)

    @classmethod
    def all_make_moves(cls, snakes):
        for snake in snakes:
            if snake._next_move is None:
                snake.active = False
            snake.make_move()

    @classmethod
    def all_check_collisions(cls, snakes, field):
        for snake in snakes:
            if snake.check_collisions(field):
                snake.active = False

    @classmethod
    def all_eat_frogs(cls, snakes, field):
        for snake in snakes:
            for frog in field.frogs:
                if snake.head == frog.coords:
                    snake.eat_frog(frog)
                    snake.score += frog.bonus


class AISnake(Snake):

    def __init__(self, head_position: Coords, length, orientation=RIGHT, name="Snake"):
        super().__init__(head_position, length, orientation, name)
        self.target = None

    def _get_target(self, field):
        """select random"""
        if not self.target and field.frogs:
            self.target = random.choice(field.frogs)

        # todo
        # sometimes change desision
        # or check if you can get there
        # or get closest

    def select_direction(self, field: Field):
        """
        self controlled snake
        :param field:
        :return:
        """
        # todo put snake inside field
        # todo refactor to make more clear algorythm
        # todo take in account place where other snakes can go
        # todo take in account other snakes tail if it gets frog - take in account its length
        # todo if field is divided, move in side of bigger part
        self._get_target(field)
        if self.target:
            # find frog
            if self.target.x == self.head.x and self.target.y > self.head.y:
                self.orientation = TOP
            elif self.target.x == self.head.x and self.target.y < self.head.y:
                self.orientation = BOTTOM
            elif self.target.x > self.head.x and self.target.y == self.head.y:
                self.orientation = RIGHT
            elif self.target.x < self.head.x and self.target.y == self.head.y:
                self.orientation = LEFT

        possible_moves = [BOTTOM, TOP, LEFT, RIGHT]

        while possible_moves:
            possible_moves.remove(self.orientation)
            next_move = self.next_move(field)

            # check is within field, and does not collide self
            if next_move is None:
                if possible_moves:
                    self.orientation = random.choice(possible_moves)
                    continue
            else:
                break


class SmartAISnake(AISnake):

    def _get_target(self, field):
        """
        each time measure distance and select closest
        :param field:
        :return:
        """
        # todo each time measure distance and select closest
        if not self.target and field.frogs:
            self.target = random.choice(field.frogs)


class PlayerSnake(Snake):

    def select_direction(self, field: Field):
        """
        self controlled snake
        :param field:
        :return:
        """

        event_match = {
            pygame.K_LEFT: LEFT,
            pygame.K_RIGHT: RIGHT,
            pygame.K_UP: TOP,
            pygame.K_DOWN: BOTTOM,
        }

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                raise KeyboardInterrupt

            if event.type == pygame.KEYUP:
                if event.key in event_match:
                    self.orientation = event_match[event.key]
