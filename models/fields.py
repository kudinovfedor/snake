import random
import itertools

from .frogs import frog_spawner_gen
from .helpers import Coords
from errors import GameOverError


class Field:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.elements = []

        self.frogs_count = 3
        self.frog_spawner = frog_spawner_gen()

        self.frogs = []
        self.snakes = []

        self.frogs_increase_interval = 20
        self.frogs_increase_in = self.frogs_increase_interval

    def __contains__(self, item: Coords):
        return (0 < item.x <= self.width) and (0 < item.y <= self.height)

    def __iter__(self):
        yield from (Coords(x, y) for x, y in itertools.product(range(1, self.width + 1), range(1, self.height + 1)))

    @property
    def active_snakes(self):
        return (i for i in self.snakes if i)

    def get_next_position(self, snake):
        """
        not transient field
        :param snake:
        :return:
        """
        return Coords(snake.head.x + snake.orientation[0], snake.head.y + snake.orientation[1])

    def get_all_empty_cells(self):
        """
        get empty cells taking in account snakes position
        :return:
        """
        empty_cells = set(self)
        # snakes
        for snake in self.snakes:
            empty_cells -= set(snake)
        # frogs
        empty_cells -= {frog.coords for frog in self.frogs}

        return list(empty_cells)

    def spawn_frogs(self):
        if len(self.frogs) < self.frogs_count:
            for _ in range(self.frogs_count - len(self.frogs)):
                self.spawn_frog()

        # solve possible deadlock
        self.frogs_increase_in -= 1
        if self.frogs_increase_in <= 0:
            self.frogs_count += 1
            self.frogs_increase_in = self.frogs_increase_interval

    def spawn_frog(self):
        """
        spawn frog taking in account snakes position
        :return:
        """
        if not self.frog_spawner:
            raise ValueError("Frog spawner generator is not set")

        free_cells = self.get_all_empty_cells()
        if not free_cells:
            return

        frog = next(self.frog_spawner)(random.choice(free_cells))
        self.frogs.append(frog)

    def update_frogs(self):
        """
        spawn new frogs based on snakes position
        make frogs older
        :return:
        """

        # multiple frogs
        for frog in self.frogs:
            if not frog:  # is not active
                self.frogs.remove(frog)
        self.spawn_frogs()


class TransientField(Field):

    def get_next_position(self, snake):
        """
        transient field
        :param snake:
        :return:
        """
        supposed_coord = super().get_next_position(snake)

        if supposed_coord not in self:
            if supposed_coord.x < 1 or supposed_coord.x > self.width:
                supposed_coord = Coords(abs(self.width - supposed_coord.x), supposed_coord.y)
            if supposed_coord.y < 1 or supposed_coord.y > self.height:
                supposed_coord = Coords(supposed_coord.x, abs(self.height - supposed_coord.y))
        return supposed_coord
