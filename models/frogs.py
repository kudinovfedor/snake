import itertools

from .helpers import Coords


class Frog:

    _value = 1
    _life_time = None
    _bonus = 100

    def __init__(self, coords: Coords):
        """
        :param coords:
        :param value:
        :param life_time: if None - infinit life_time
        """
        self.active = True
        self.coords = coords

        self.value = self._value
        self.life_time = self._life_time
        self.bonus = self._bonus

    @property
    def x(self):
        return self.coords.x

    @property
    def y(self):
        return self.coords.y

    def reduce_life_time(self):
        if self.life_time:
            self.life_time -= 1
            if self.life_time <= 0:
                self.active = False

    def __bool__(self):
        return self.active

    @classmethod
    def all_reduce_life_time(cls, frogs):
        for frog in frogs:
            if frog:  # is active
                frog.reduce_life_time()


class BigFrog(Frog):

    _value = 3
    _life_time = 7
    _bonus = 300


def frog_spawner_gen():
    loop = [Frog] * 4 + [BigFrog]
    yield from itertools.cycle(loop)
