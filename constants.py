LEFT = (-1, 0)
RIGHT = (1, 0)
TOP = (0, 1)
BOTTOM = (0, -1)

AI_SNAKE = "AISnake"
PLAYER_SNAKE = "PlayerSnake"

CONSOLE_VIEW = "View"
PYGAME_VIEW = "PygameView"

SIMPLE_FIELD = "Field"
TRANSIENT_FIELD = "TransientField"
